package com.gitlab.sapujapu.picar;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector3;
import com.gitlab.sapujapu.picar.TCP.MessageReceivedListener;
import com.gitlab.sapujapu.picar.TCP.TCPClient;

import java.io.IOException;

public class Main extends ApplicationAdapter implements InputProcessor, ControllerListener {

	private TCPClient client;

	@Override
	public void create() {
		//batch = new SpriteBatch();
		//img = new Texture("badlogic.jpg");
		Gdx.input.setInputProcessor(this);
		Controllers.addListener(this);

		try {
			client = new TCPClient("raspberrypi.fritz.box", 8085);
			client.addMessageReceivedListener(new MessageReceivedListener() {
				@Override
				public void onMessageReceive(String message) {
					System.out.println(message);
				}
			});

			client.start();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//batch.begin();
		//batch.draw(img, 0, 0);
		//batch.end();
	}

	@Override
	public boolean keyDown(int keycode) {
		System.out.println(keycode);
		switch (keycode) {
			case Input.Keys.UP:
				try {
					client.send("1");
				} catch (IOException e) {
					e.printStackTrace();
				}
			case Input.Keys.DOWN:
				try {
					client.send("-1");
				} catch (IOException e) {
					e.printStackTrace();
				}
			case Input.Keys.SPACE:
				try {
					client.send("0");
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	//region controller
	@Override
	public void connected(Controller controller) {

	}

	@Override
	public void disconnected(Controller controller) {

	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		return false;
	}

	@Override
	public boolean buttonUp(Controller controller, int buttonCode) {
		return false;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
		//System.out.println("controller = [" + controller + "], axisCode = [" + axisCode + "], value = [" + value + "]");
		return false;
	}

	@Override
	public boolean povMoved(Controller controller, int povCode, PovDirection value) {
		//System.out.println("controller = [" + controller + "], povCode = [" + povCode + "], value = [" + value + "]");
		if(value.equals(PovDirection.north)){
			try {
				client.send("1");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(value.equals(PovDirection.south)){
			try {
				client.send("-1");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(value.equals(PovDirection.center)){
			try {
				client.send("0");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	@Override
	public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
		return false;
	}

	@Override
	public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
		return false;
	}
//endregion
}