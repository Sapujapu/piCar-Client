package com.gitlab.sapujapu.picar.TCP;

public interface MessageReceivedListener {

    void onMessageReceive(String message);
}
