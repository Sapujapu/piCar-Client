package com.gitlab.sapujapu.picar.TCP;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

//Testing the client
/*
class ClientTest implements MessageReceivedListener
{
    public ClientTest() throws IOException {
        TCPClient TCPClient = new TCPClient();
        TCPClient.addMessageReceivedListener(this);
        TCPClient.start();
    }
    public static void main(String argv[]) throws Exception{
        ClientTest clientTest = new ClientTest();
    }

    @Override
    public void onMessageReceive(String message) {
        System.out.println(message);
    }
}*/

class MessageReceiver implements Runnable{
    List<MessageReceivedListener> messageReceivedListeners = new ArrayList<MessageReceivedListener>();
    BufferedReader inFromServer = null;

    private MessageReceiver(){

    }
    public MessageReceiver(BufferedReader in){
        inFromServer = in;
    }

    public void addMessageReceiveListener(MessageReceivedListener listener){
        messageReceivedListeners.add(listener);
    }

    @Override
    public void run() {
        while(true){
            String received = null;
            try {

                received = inFromServer.readLine();

                for(MessageReceivedListener listener : messageReceivedListeners){
                    listener.onMessageReceive(received);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}

public class TCPClient {

    private MessageReceiver messageReceiver;
    DataOutputStream toServer;
    public TCPClient(String ip,int port) throws IOException {

        Socket clientSocket = new Socket(ip, port);
        toServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        messageReceiver = new MessageReceiver(fromServer);
    }

    public void start() throws IOException {

        Thread t = new Thread(messageReceiver);

        t.start();
/*
        while (true) {

            String input = new Scanner(System.in).next();
            toServer.writeBytes(input + "\n");
        }*/
    }

    public void addMessageReceivedListener(MessageReceivedListener listener){
        messageReceiver.addMessageReceiveListener(listener);
    }

    public void send(String message) throws IOException {

        toServer.writeBytes(message + "\n");

    }

}